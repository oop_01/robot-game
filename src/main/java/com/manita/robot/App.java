package com.manita.robot;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    static Map map = new Map(15, 15);
    static Robot robot = new Robot(map, 10, 10, 'A');
    static Tree Tree1 = new Tree(map, 5, 5);
    static Tree Tree2 = new Tree(map, 5, 6);
    static Tree Tree3 = new Tree(map, 6, 5);
    static Tree Tree4 = new Tree(map, 7, 5);
    static Tree Tree5 = new Tree(map, 7, 7);
    static Tree Tree6 = new Tree(map, 6, 7);
    static Tree Tree7 = new Tree(map, 11, 9);
    static Tree Tree8 = new Tree(map, 11, 10);
    static Tree Tree9 = new Tree(map, 11, 12);
    static Tree Tree10 = new Tree(map, 12, 10);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "w":
                robot.up();
                break;
            case "s":
                robot.down();
                break;
            case "a":
                robot.left();
                break;
            case "d":
                robot.right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        map.add(Tree1);
        map.add(Tree2);
        map.add(Tree3);
        map.add(Tree4);
        map.add(Tree5);
        map.add(Tree6);
        map.add(Tree7);
        map.add(Tree8);
        map.add(Tree9);
        map.add(Tree10);
        map.add(robot);

        while (true) {
            map.print();
            String command = input();
            process(command);
        }
    }
}
